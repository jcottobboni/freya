$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "freya/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "freya"
  s.version     = Freya::VERSION
  s.authors     = ["jcottobboni"]
  s.email       = ["jcottobboni@gmail.com"]
  s.homepage    = "http://www.jcottobboni.com.br"
  s.summary     = "backend layout engine"
  s.description = "backedn layout engine"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails"

end
